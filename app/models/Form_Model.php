<?php

/**
 * Created by IntelliJ IDEA.
 * User: DELL
 * Date: 06-Nov-15
 * Time: 4:23 PM
 */
class Form_Model extends Controller
{
	public $user_id;
	public $error_msg = null;

	public function index()
	{
		echo 'form_model index';
	}

	public function add_user()
	{
		$login = $_POST['login'];
		$password = $_POST['password'];
		$statement = $this->db->prepare("SELECT id FROM users
            WHERE login=:login");
		$statement->execute(array(
			':login' => $login
		));
		$count = $statement->rowCount();
		if ($count > 0)
		{
			$this->error_msg = 'User alraedy existed, will not add a new user of the same login';
		}
		else
		{
			$statement = $this->db->prepare("INSERT INTO users (login, password) VALUES (:login, :password)");
			$outcome = $statement->execute(array(
				':login' =>$login,
				':password' => md5($password)
			));

			if($outcome) {
				echo 'added a new user';
				header('location: /mvc_form/public/form/');
			}

		}


	}
}